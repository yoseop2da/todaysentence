//
//  Today.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "Today.h"

@implementation Today

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self)
    {
        self.dateKey = [decoder decodeObjectForKey:@"dateKey"];
        self.locale = [decoder decodeObjectForKey:@"locale"];
        self.krSentence = [decoder decodeObjectForKey:@"krSentence"];
        self.enSentence = [decoder decodeObjectForKey:@"enSentence"];
        self.jpSentence = [decoder decodeObjectForKey:@"jpSentence"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.dateKey forKey:@"dateKey"];
    [encoder encodeObject:self.locale forKey:@"locale"];
    [encoder encodeObject:self.krSentence forKey:@"krSentence"];
    [encoder encodeObject:self.enSentence forKey:@"enSentence"];
    [encoder encodeObject:self.jpSentence forKey:@"jpSentence"];
}


@end
