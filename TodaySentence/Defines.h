//
//  Defines.h
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

#define SettingLocaleKR @"KR"
#define SettingLocaleEN @"EN"
#define SettingLocaleJP @"JP"


#define USERDEFAULT_FIRST_SETUP_FINISHED @"isFirstSetupFinished"
#define USERDEFAULT_KR_ARRAY @"KRSentenceArray"
#define USERDEFAULT_EN_ARRAY @"ENSentenceArray"
#define USERDEFAULT_JP_ARRAY @"JPSentenceArray"

#define USERDEFAULT_HISTORY @"historyModelData"

#define USERDEFAULT_SETTINGS @"SettingsModel"

#endif /* Defines_h */

