//
//  ViewController.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()
{
    __weak IBOutlet UILabel *sentenceLabel;
    __weak IBOutlet UILabel *writtenByLabel;
    
    __weak IBOutlet UIButton *shareButton;
    __weak IBOutlet UIButton *historyButton;
    __weak IBOutlet UIButton *settingsButton;
    
    
}
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - initialize
- (void)initialize
{
    [self buttonShapeToRound];
    
    [self setTodaySentence];
}

- (void)buttonShapeToRound
{
    shareButton.layer.cornerRadius = 25.f;
    historyButton.layer.cornerRadius = 25.f;
    settingsButton.layer.cornerRadius = 25.f;
}

#pragma mark - Button Actions

- (IBAction)shareButtonTouched:(id)sender
{
    
}

- (IBAction)historyButtonTouched:(id)sender
{
    
}

- (IBAction)settingsButtonTouched:(id)sender
{
    
}

#pragma mark -

- (void)setTodaySentence
{
    sentenceLabel.text = [[MainManager instance] mainSentence].sentence;
    writtenByLabel.text = [[MainManager instance] mainSentence].writtenBy;
}

#pragma mark - StatusBar Hidden.

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
@end
