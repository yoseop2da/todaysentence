//
//  Today.h
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Today : NSObject

@property (strong) NSString *dateKey;
@property (strong) NSString *locale;
@property (strong) Sentence *krSentence;
@property (strong) Sentence *enSentence;
@property (strong) Sentence *jpSentence;

@end
