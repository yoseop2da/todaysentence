//
//  Sentence.h
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sentence : NSObject

@property (strong, nonatomic) NSString *sentence;
@property (strong, nonatomic) NSString *writtenBy;
@property (strong, nonatomic) NSString *locale;

-(instancetype)initWithDictionary:(NSDictionary *)dic;

@end
