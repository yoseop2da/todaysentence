//
//  MainManager.h
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainManager : NSObject

+(id)instance;

- (Sentence *)mainSentence;

@end
