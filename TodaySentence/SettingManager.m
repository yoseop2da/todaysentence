//
//  SettingManager.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "SettingManager.h"

@implementation SettingManager


+(id)instance
{
    static SettingManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    
    return instance;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self loadSettings];
    }
    return self;
}

- (void)loadSettings
{
    NSData *settingsData = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_SETTINGS];
    if (settingsData) {
        self.setting = [NSKeyedUnarchiver unarchiveObjectWithData:settingsData];
    }else{
        self.setting = [Setting new];
    }
}
@end
