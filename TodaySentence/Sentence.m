//
//  Sentence.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "Sentence.h"

@implementation Sentence

-(instancetype)initWithDictionary:(NSDictionary *)dic
{
    if (self = [super init]) {
        self.sentence = dic[@"sentence"];
        self.writtenBy = dic[@"writtenBy"];
        self.locale = dic[@"locale"];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self)
    {
        self.sentence = [decoder decodeObjectForKey:@"sentence"];
        self.writtenBy = [decoder decodeObjectForKey:@"writtenBy"];
        self.locale = [decoder decodeObjectForKey:@"locale"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.sentence forKey:@"sentence"];
    [encoder encodeObject:self.writtenBy forKey:@"writtenBy"];
    [encoder encodeObject:self.locale forKey:@"locale"];
}

@end
