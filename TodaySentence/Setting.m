//
//  Setting.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "Setting.h"

@implementation Setting

- (instancetype)init
{
    if (self = [super init]) {
        _defaultlocale = SettingLocaleKR;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self)
    {
        self.defaultlocale = [decoder decodeObjectForKey:@"defaultlocale"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.defaultlocale forKey:@"defaultlocale"];
}

@end
