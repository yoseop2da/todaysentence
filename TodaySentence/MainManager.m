//
//  MainManager.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "MainManager.h"
@interface MainManager()
{
    NSArray *_krSentences;
    NSArray *_enSentences;
    NSArray *_jpSentences;
    
    Sentence *_todayKR;
    Sentence *_todayEN;
    Sentence *_todayJP;
    
    Today *_today;
}
@end

@implementation MainManager

+(id)instance
{
    static MainManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    
    return instance;
}

-(instancetype)init
{
    if (self = [super init]) {
        [self loadJSon];
    }
    return self;
}

- (void)loadJSon
{
    BOOL isAlreadySetup = [[NSUserDefaults standardUserDefaults] boolForKey:USERDEFAULT_FIRST_SETUP_FINISHED];
    if (isAlreadySetup) {
        _krSentences = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_KR_ARRAY];
        _enSentences = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_EN_ARRAY];
        _jpSentences = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_JP_ARRAY];
    }else{
        NSLog(@"초기 셋팅해주는곳..... 최초에 한번만 이곳이 호출됩니다.");

        _krSentences = [self sentencesWithResourceName:@"sentence_kr"];
        _enSentences = [self sentencesWithResourceName:@"sentence_en"];
        _jpSentences = [self sentencesWithResourceName:@"sentence_jp"];
        
        [self saveKRWithArray:_krSentences];
        [self saveENWithArray:_enSentences];
        [self saveJPWithArray:_jpSentences];
        
        // 셋팅 완료후, BOOL값 변경해주기.
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USERDEFAULT_FIRST_SETUP_FINISHED];
    }
}

- (Sentence *)mainSentence
{
    if (!_today) {
        NSString *key = [self loadTodayKey];
        
        if (![self isTodayDataAlreadySet:key]) {
            int value = (int)arc4random_uniform((int)_krSentences.count-1);
            _todayKR = [[Sentence alloc] initWithDictionary:_krSentences[value]];
            _todayEN = [[Sentence alloc] initWithDictionary:_enSentences[value]];
            _todayJP = [[Sentence alloc] initWithDictionary:_jpSentences[value]];
            
            NSMutableArray *changedKRArray = [_krSentences mutableCopy];
            [changedKRArray removeObjectAtIndex:value];
            [self saveKRWithArray:changedKRArray];
            _krSentences = changedKRArray;
            
            NSMutableArray *changedENArray = [_enSentences mutableCopy];
            [changedENArray removeObjectAtIndex:value];
            [self saveENWithArray:changedENArray];
            _enSentences = changedENArray;
            
            NSMutableArray *changedJPArray = [_jpSentences mutableCopy];
            [changedJPArray removeObjectAtIndex:value];
            [self saveJPWithArray:changedJPArray];
            _jpSentences = changedJPArray;
            
            _today = [Today new];
            _today.dateKey = key;
            _today.locale = [[SettingManager instance] setting].defaultlocale;
            _today.krSentence = _todayKR;
            _today.enSentence = _todayEN;
            _today.jpSentence = _todayJP;
            
            NSMutableArray *historyArray;
            NSData *historyData = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_HISTORY];
            if (historyData) {
                historyArray = [[NSKeyedUnarchiver unarchiveObjectWithData:historyData] mutableCopy];
            }else{
                historyArray = [NSMutableArray array];
            }
            
            [historyArray addObject:_today];
            historyData = [NSKeyedArchiver archivedDataWithRootObject:historyArray];
            [[NSUserDefaults standardUserDefaults] setObject:historyData forKey:USERDEFAULT_HISTORY];
        }
    }else{
        
    }
    
    if ([[[SettingManager instance] setting].defaultlocale isEqualToString:SettingLocaleKR]) {
        return _today.krSentence;
    }else if([[[SettingManager instance] setting].defaultlocale isEqualToString:SettingLocaleEN]) {
        return _today.enSentence;
    }else{
        return _today.jpSentence;
    }
}


- (NSString *)loadTodayKey
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateformatter = [NSDateFormatter new];
    dateformatter.dateFormat = @"yyyyMMdd";
    return [dateformatter stringFromDate:date];
}

- (void)saveKRWithArray:(NSArray *)array
{
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:USERDEFAULT_KR_ARRAY];
}

- (void)saveENWithArray:(NSArray *)array
{
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:USERDEFAULT_EN_ARRAY];
}

- (void)saveJPWithArray:(NSArray *)array
{
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:USERDEFAULT_JP_ARRAY];
}

- (NSArray *)sentencesWithResourceName:(NSString *)resource
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:resource ofType:@"json"];
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    return json[@"sentenceArray"];
}

- (BOOL)isTodayDataAlreadySet:(NSString *)key
{
    NSMutableArray *historyArray;
    NSData *historyData = [[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_HISTORY];
    if (historyData) {
        historyArray = [[NSKeyedUnarchiver unarchiveObjectWithData:historyData] mutableCopy];
    }else{
        historyArray = [NSMutableArray array];
    }
    
    for (Today *today in historyArray) {
        if([today.dateKey isEqualToString:key]){
            _today = today;
            return YES;
        }
    }
    
    return NO;
}
@end
