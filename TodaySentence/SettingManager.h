//
//  SettingManager.h
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 11..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingManager : NSObject

@property (strong) Setting *setting;

+(id)instance;


@end
